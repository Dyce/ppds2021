from fei.ppds import Thread, Mutex


class Shared():
    def __init__(self, end):
        self.counter = 0
        self.end = end
        self.array = [0] * self.end # inicializuje pole na 0
        self.mutex = Mutex()


class Histogram(dict):
    def __init__(self, seq=[]):
        for item in seq:
            self[item] = self.get(item, 0) + 1

# pripad A - v tomto pripade sa za kriticku oblast povazuje cely cklus. Je to nutne, aby nedoslo pocas behu vlakien
#           k zmene hodnoty shared.counter po overeni podmienky. Toto bol dovod index out of range chyby na seminári
#           tento sposob je sice najpomalsi, ale je nutny pre korektnu pracu programu

# pripad B - za kriticku oblast sa povazuje inkrementacia hodnoty v poli a zvysenie hodnoty counter. Tymto sposobom
#           je mozne predist hodnotam 2 a viac na danych indexoch, nepredideme vsak index out of range chybe.
#           ta nastane z toho dovodu, ze po overeni podmienky ine vlakno zvysi hodnotu counteru. Pri tejto hodnote
#           by podmienka bola false a vykonavanie by sa ukoncilo, avsak niektore z vlakien budu po prebudeni pokracovat
#           vykonavanim programu ZA danou podmienkou. Histogram obsahoval len hodnoty 1, avsak vlakna vykazovali
#           chyb index out of range

# pripad C - kriticka oblast je len zvysenie hodnoty pocitadla. V tomto pripade mozeme zabranit tomu, aby viacero
#           vlakien naraz zvysilo tuto hodnotu. Kedze vsak s touto hodnotou pracujeme aj inde vo funkcii, jej hodnota
#           sa moze pocas behu funkcie v danom vlakne necakane zmenit. Vystup histogramu teda ocakavane obsahoval
#           aj indexy s hodnotou 2


def counter(shared):
    while True:
        shared.mutex.lock()  # pripad A
        if shared.counter >= shared.end:
            shared.mutex.unlock()  # pripad A
            break
        # shared.mutex.lock()  # pripad B
        shared.array[shared.counter] += 1
        # shared.mutex.lock()  # pripad C
        shared.counter += 1
        shared.mutex.unlock()


for i in range(10):
    print(i)
    sh = Shared(1000000)
    # 1. arg - funkcia co sa bude vykonavat
    # 2. arg - vstupny parameter
    t1 = Thread(counter, sh)
    t2 = Thread(counter, sh)

    t1.join()
    t2.join()

    print(Histogram(sh.array))